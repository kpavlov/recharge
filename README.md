# Recharger ⛽ [![pipeline status](https://gitlab.com/kpavlov/recharge/badges/java/pipeline.svg)](https://gitlab.com/kpavlov/recharge/commits/master)

## Model

_Transaction_ represents the charging process on the charging station. 
It has following fields:
    
  * id - unique identifier of the transaction;
  * stationId - the id of the station where charging happened;
  * startedAt - timestamp which indicates when the transaction started;
  * status - the status of the transaction indicates if it is in progress or finished.
  * endedAt - timestamp which indicates when the transaction ended;
  * consumption - resource consumption;
  

## REST API Endpoints

There are following API endpoints defined ([OAS3](./src/main/resources/transactions.oas3.yaml)):

 * `POST /transactions` – submit a new transaction for the station. The complexity of storing the transaction must be O(1);
 * `PUT /transaction` – stops provided in the body transaction. The complexity of the operation must be O(1);
 * `GET /transactions` – retrieves a total list of transactions including started and stopped items with info about the total count, total started and total stopped transactions for the last minute. Complexity of retrieval must be O(1);
4. The following endpoints could be implemented:
 * `GET /transactions/stopped` – retrieves list of transactions stopped during last minute.
Complexity of retrieval must be O(1);
 * `GET /transactions/started` – retrieves list of transactions started during last minute.
Complexity of retrieval must be O(1);
 * `DELETE /transactions` – removes stopped transactions. Complexity must be O(1).

Notes:

* Pagination in transaction list is out of scope. It may not be a requirement given that 
only transactions for last minute are requested.
* Server is responsible for assigning timestamps and IDs

## Build and Run

Build it with maven:

    mvn clean verify
    
Run with

    java -jar ./target/app-exec.jar
    
Or just use the script:

    ./run-me.sh
