openapi: 3.0.1
info:
  title: Transactions API
  description: API for managing transactions for recent minute
  version: 1.0.0
servers:
  - url: 'http://localhost:8080'

paths:
  /transactions:
    get:
      operationId: listTransactions
      summary: Returns a total list of transactions.
      description: Retrieves a total list of transactions including started and stopped items with info about the total count, total started and total stopped transactions for the last minute..
      tags:
        - transactions
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TransactionListResponse'
        default:
          $ref: '#/components/responses/ErrorResponse'
    post:
      summary: Submit a new transaction for the station.
      operationId: submitTransaction
      tags:
        - transactions
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SubmitTransactionRequest'
      responses:
        201:
          description: TransactionModel was created
          headers:
            Location:
              description: TransactionModel API resource URL
              required: true
              schema:
                type: string
                example: '/transactions/10d56b20-c3ba-4545-8926-71edcac0fd49'
        default:
          $ref: '#/components/responses/ErrorResponse'
    delete:
      summary: Delete stopped transactions
      operationId: deleteStoppedTransactions
      tags:
        - transactions
      responses:
        204:
          description: The action has been performed
        default:
          $ref: '#/components/responses/ErrorResponse'

  /transaction:
    put:
      summary: Stops provided in the body transaction
      operationId: stopTransaction
      tags:
        - transactions
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/StopTransactionRequest'
      responses:
        200:
          description: TransactionModel was stopped
        400:
          description: Bad request
        404:
          description: TransactionModel not found or it was started more than one minute ago
        409:
          description: TransactionModel was already stopped

  /transactions/started:
    get:
      summary: Get started transactions
      operationId: listStartedTransactions
      description: Retrieves list of transactions started during last minute
      tags:
        - transactions
      responses:
        200:
          description: Stared transaction list
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TransactionListResponse'
        default:
          $ref: '#/components/responses/ErrorResponse'

  /transactions/stopped:
    get:
      summary: Get stopped transactions
      operationId: listStoppedTransactions
      description: Retrieves list of transactions stopped during last minute
      tags:
        - transactions
      responses:
        200:
          description: Stopped transaction list
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TransactionListResponse'
        default:
          $ref: '#/components/responses/ErrorResponse'

components:
  schemas:
    TransactionId:
      type: string
      format: uuid
      description: Unique identifier of the transaction
      example: 10d56b20-c3ba-4545-8926-71edcac0fd49
    StationId:
      type: integer
      format: int32
      description: The id of the station where charging happened
      example: 12345
    Consumption:
      description: Resource consumption. Only for finished transactions.
      type: integer
      format: int64
      minimum: 0
      nullable: true
      example: 100500

    Transaction:
      properties:
        id:
          $ref: '#/components/schemas/TransactionId'
        stationId:
          $ref: '#/components/schemas/StationId'
        status:
          description: The status of the transaction indicates if it is in progress or finished.
          type: string
          enum:
            - IN_PROGRESS
            - FINISHED
          default: IN_PROGRESS
        startedAt:
          description: Timestamp which indicates when the transaction started
          type: string
          format: 'date-time'
          example: '2019-01-10T05:48:56:901Z'
        endedAt:
          description: Timestamp which indicates when the transaction ended. Only for finished transactions.
          type: string
          format: 'date-time'
          example: '2019-01-10T07:31:12:244Z'
        consumption:
          $ref: '#/components/schemas/Consumption'
      required:
        - id
        - stationId
        - status
        - startedAt

    SubmitTransactionRequest:
      description: TransactionModel data resuired to submit a transaction
      properties:
        stationId:
          $ref: '#/components/schemas/StationId'
      required:
        - stationId

    Totals:
      properties:
        totalCount:
          description: Total transaction count
          type: integer
          minimum: 0
          format: int32
          example: 42
        totalStarted:
          description: Total started transactions
          type: integer
          minimum: 0
          format: int32
          example: 20
        totalStopped:
          description: Total stopped transactions
          type: integer
          minimum: 0
          format: int32
          example: 22
      required:
        - totalCount
        - totalStarted
        - totalStopped

    TransactionListResponse:
      description: List of transactions including totals
      properties:
        meta:
          $ref: '#/components/schemas/Totals'
        data:
          description: Transactions
          type: array
          items:
            $ref: '#/components/schemas/Transaction'
      required:
        - meta
        - data
    StopTransactionRequest:
      properties:
        id:
          $ref: '#/components/schemas/TransactionId'
        consumption:
          $ref: '#/components/schemas/Consumption'
      required:
        - id
        - consumption

  responses:
    ErrorResponse:
      description: Error response

