package com.example.recharge.domain;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

import java.time.OffsetDateTime;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

@ThreadSafe

public final class TransactionModel {

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    @GuardedBy("lock")
    private OffsetDateTime endTimestamp;
    @GuardedBy("lock")
    private Long consumptionValue;
    private final UUID id;
    private final int stationId;
    private final OffsetDateTime startedAt;

    public final OffsetDateTime getEndedAt() {
        ReadLock readLock = lock.readLock();
        readLock.lock();
        try {
            return this.endTimestamp;
        } finally {
            readLock.unlock();
        }
    }

    public final Long getConsumption() {
        ReadLock readLock = lock.readLock();
        readLock.lock();
        try {
            return this.consumptionValue;
        } finally {
            readLock.unlock();
        }
    }

    public final boolean isFinished() {
        return this.getEndedAt() != null;
    }

    public final void finish(OffsetDateTime timestamp, long consumed) {
        WriteLock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            if (timestamp.isBefore(this.startedAt)) {
                throw new IllegalArgumentException("Finish timestamp is before start timestamp");
            }

            if (this.endTimestamp != null) {
                throw new IllegalStateException("Can't set value twice");
            }

            this.endTimestamp = timestamp;
            this.consumptionValue = consumed;
        } finally {
            writeLock.unlock();
        }

    }

    public final UUID getId() {
        return this.id;
    }

    public final int getStationId() {
        return this.stationId;
    }

    public final OffsetDateTime getStartedAt() {
        return this.startedAt;
    }

    public TransactionModel(UUID id, int stationId, OffsetDateTime startedAt) {
        this.id = id;
        this.stationId = stationId;
        this.startedAt = startedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionModel that = (TransactionModel) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
