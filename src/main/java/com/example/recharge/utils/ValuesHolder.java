package com.example.recharge.utils;

import net.jcip.annotations.ThreadSafe;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

@ThreadSafe
public class ValuesHolder<ID, T> {

    private final Map<ID, T> data = new ConcurrentHashMap<>();
    private final long epochSeconds;

    public ValuesHolder(long epochSeconds) {
        this.epochSeconds = epochSeconds;
    }

    public long getEpochSeconds() {
        return epochSeconds;
    }

    public T put(ID key, T value) {
        return data.put(key, value);
    }

    public T get(ID key) {
        return data.get(key);
    }

    public Collection<T> values() {
        return data.values();
    }

    public void removeIf(BiFunction<ID, T, Boolean> predicate) {
        data.entrySet().removeIf(entry -> predicate.apply(entry.getKey(), entry.getValue()));
    }

    public Set<ID> keys() {
        return data.keySet();
    }
}
