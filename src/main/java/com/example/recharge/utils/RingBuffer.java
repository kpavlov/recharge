package com.example.recharge.utils;

import net.jcip.annotations.NotThreadSafe;

import java.util.function.Function;
import java.util.stream.Stream;

/**
 * RingBuffer is not thread-safe Circular Buffer.
 * <p>
 *
 * @implNote NOTE: The implantation is NOT THREAD-SAFE
 * @link https://en.wikipedia.org/wiki/Circular_buffer
 */
@NotThreadSafe
public class RingBuffer<T> {
    private final T[] elements;
    private int offset = 0;
    private final Function<Integer, T> initializer;

    /**
     * * @param size        buffer size
     * * @param initializer Function that initializes new buffer elements. Accepts element index
     */
    public RingBuffer(int size, Function<Integer, T> initializer) {
        //noinspection unchecked
        this.elements = (T[]) new Object[size];
        this.initializer = initializer;

        for (int i = 0; i < elements.length; ++i) {
            elements[i] = initializer.apply(i);
        }
    }

    public final T get(int index) {
        return elements[getArrayIndex(index)];
    }

    public Stream<T> stream() {
        final Stream.Builder<T> streamBuilder = Stream.builder();
        for (int i = elements.length - 1; i >= 0; --i) {
            final T value = get(i);
            streamBuilder.add(value);
        }
        return streamBuilder.build();
    }

    public final void advance(int delta, Function<Integer, T> initializer) {
        if (delta >= elements.length) {
            this.reset(initializer);
        } else {
            offset -= delta;
            if (offset < 0) {
                offset = elements.length - delta;
            }

            for (int i = 0; i < delta; i++) {
                elements[offset + i] = initializer.apply(delta - i - 1);
            }
        }
    }

    /**
     * Clears (reset) buffer by re-initializing all it's elements
     *
     * @param initializer optional initializer function
     */
    public void reset(Function<Integer, T> initializer) {
        offset = 0;
        for (int i = 0; i < elements.length; ++i) {
            elements[i] = initializer.apply(i);
        }
    }

    void reset() {
        reset(initializer);
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < elements.length; i++) {
            if (i > 0) {
                buffer.append(", ");
            }

            buffer.append(get(i));
        }

        return "{offset=" + offset + ", [" + buffer + "]}";
    }

    private int getArrayIndex(int index) {
        return (index + elements.length + offset) % elements.length;
    }

    public final int getSize() {
        return elements.length;
    }

}
