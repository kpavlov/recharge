package com.example.recharge.utils;

import java.time.Clock;

public final class Utils {
    private Utils() {
    }

    public static long getEpochSecond(Clock clock) {
        return clock.millis() / 1000;
    }
}
