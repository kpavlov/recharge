package com.example.recharge.utils;

import com.example.recharge.service.TransactionInTheFutureException;
import com.example.recharge.service.TransactionTooOldException;
import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

import java.time.Clock;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.Math.min;

/**
 * Representing time interval with second-accuracy and ValuesHolder "attached" to every second.
 *
 * @see ValuesHolder
 */
@ThreadSafe
public class TimeInterval<ID, T> {

    private final Clock clock;
    private final RingBuffer<ValuesHolder<ID, T>> buffer;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
    private final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private final int lengthInSeconds;

    /**
     * @param lengthInSeconds   how many seconds are in interval. Default is 60
     * @param clock             a Clock (time provider)
     */
    public TimeInterval(int lengthInSeconds,
                        Clock clock) {
        this.clock = clock;
        this.lengthInSeconds = lengthInSeconds;

        final long epochSecondsNow = Utils.getEpochSecond(clock);
        buffer = new RingBuffer<>(lengthInSeconds, it -> new ValuesHolder<>(epochSecondsNow - it));
    }

    @GuardedBy("lock")
    public void submit(OffsetDateTime timestamp, ID key, T value) throws TransactionTooOldException, TransactionInTheFutureException {
        final long timestampMillis = timestamp.toInstant().toEpochMilli();
        final long currentMillis = clock.millis();
        if (timestampMillis > currentMillis) {
            throw new TransactionInTheFutureException(timestamp);
        }
        final long epochSecondsNow = currentMillis / 1000;
        final long epochSecond = timestamp.toEpochSecond();
        final int offset = (int) (epochSecondsNow - epochSecond);
        if (offset >= lengthInSeconds) {
            throw new TransactionTooOldException(timestamp);
        }

        boolean holdsReadLock = false;
        try {
            holdsReadLock = advanceRingBufferAndGetReadLock(epochSecondsNow);
            final ValuesHolder<ID, T> holder = buffer.get(offset);
            holder.put(key, value);
        } finally {
            if (holdsReadLock) {
                readLock.unlock();
            }
        }

    }

    @GuardedBy("lock")
    public void reset() {
        writeLock.lock();
        try {
            buffer.reset();
        } finally {
            writeLock.unlock();
        }
    }

    @GuardedBy("lock")
    public List<T> extract(Predicate<T> predicate) {
        final long epochSecondsNow = Utils.getEpochSecond(clock);
        boolean holdsReadLock = false;
        try {
            holdsReadLock = advanceRingBufferAndGetReadLock(epochSecondsNow);
            long minSecond = epochSecondsNow - lengthInSeconds;
            return buffer
                .stream()
                .filter(it -> it.getEpochSeconds() >= minSecond)
                .flatMap(h -> h.values().stream().filter(predicate))
                .collect(Collectors.toList());
        } finally {
            if (holdsReadLock) {
                readLock.unlock();
            }
        }
    }

    @GuardedBy("lock")
    public Optional<T> findById(ID id) {
        final long epochSecondsNow = Utils.getEpochSecond(clock);
        boolean holdsReadLock = false;
        try {
            holdsReadLock = advanceRingBufferAndGetReadLock(epochSecondsNow);
            long minSecond = epochSecondsNow - lengthInSeconds;
            return buffer
                .stream()
                .filter(it -> it.getEpochSeconds() >= minSecond)
                .map(h -> h.get(id))
                .filter(Objects::nonNull)
                .findAny();
        } finally {
            if (holdsReadLock) {
                readLock.unlock();
            }
        }
    }

    @GuardedBy("lock")
    public void forEachSecond(Consumer<ValuesHolder<ID, T>> operation) {
        readLock.lock();
        try {
            for (int i = 0; i < buffer.getSize(); i++) {
                operation.accept(buffer.get(i));
            }
        } finally {
            readLock.unlock();
        }
    }

    private boolean advanceRingBufferAndGetReadLock(long epochSecondsNow) {
        writeLock.lock();
        try {
            final long bufferSecond = buffer.get(0).getEpochSeconds();
            final int delta = (int) (epochSecondsNow - bufferSecond);
            if (delta != 0L) {
                System.out.println("Advance by " + delta);
                final int offset = min(delta, lengthInSeconds);
                buffer.advance(offset, (it) -> new ValuesHolder<>(epochSecondsNow - it));
            }
            readLock.lock();
            return true;
        } catch (Throwable e) {
            return false;
        } finally {
            writeLock.unlock();
        }
    }
}
