package com.example.recharge.service;

import com.example.recharge.domain.TransactionModel;
import com.example.recharge.utils.TimeInterval;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.util.UUID;


@Configuration
class ServiceConfig {

    private final Clock clock = Clock.systemUTC();
    private final int INTERVAL_LENGTH_SECONDS = 60;

    private final TimeInterval<UUID, TransactionModel> minuteInterval = new TimeInterval<>(
        INTERVAL_LENGTH_SECONDS,
        clock
    );

    @Bean
    TransactionService transactionService() {
        return new TransactionService(minuteInterval);
    }

    @Bean
    Clock clock() {
        return clock;
    }

    @Bean
    TimeInterval<UUID, TransactionModel> minuteInterval() {
        return minuteInterval;
    }
}
