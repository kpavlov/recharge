package com.example.recharge.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.OffsetDateTime;


@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "TransactionModel date is in the future")
public class TransactionInTheFutureException extends RuntimeException {
    public TransactionInTheFutureException(OffsetDateTime timestamp) {
        super("TransactionModel date is in the future: " + timestamp);
    }
}
