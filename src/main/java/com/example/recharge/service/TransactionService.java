package com.example.recharge.service;

import com.example.recharge.domain.TransactionModel;
import com.example.recharge.utils.TimeInterval;

import java.time.OffsetDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class TransactionService {

    private final TimeInterval<UUID, TransactionModel> interval;

    public TransactionService(TimeInterval<UUID, TransactionModel> interval) {
        this.interval = Objects.requireNonNull(interval);
    }

    public UUID createTransaction(OffsetDateTime timestamp, int stationId) {
        Objects.requireNonNull(timestamp, "timestamp");
        UUID transactionId = UUID.randomUUID();
        TransactionModel transaction = new TransactionModel(transactionId, stationId, timestamp);
        interval.submit(timestamp, transactionId, transaction);
        return transactionId;
    }

    public void stopTransaction(UUID id, OffsetDateTime timestamp, long consumption) {
        Objects.requireNonNull(id, "id");
        Objects.requireNonNull(timestamp, "timestamp");
        TransactionModel transaction = getTransaction(id);
        transaction.finish(timestamp, consumption);
        try {
            interval.submit(timestamp, id, transaction);
        } catch (TransactionTooOldException e) {
            // we don't care about it
        }
    }

    private TransactionModel getTransaction(final UUID id) throws TransactionNotFoundException {
        return interval
            .findById(id)
            .orElseThrow(() -> new TransactionNotFoundException(id));
    }

    public final List<TransactionModel> listTransactions() {
        final Set<UUID> ids = new LinkedHashSet<>();
        return interval.extract(it -> ids.add(it.getId()));
    }

    public final List<TransactionModel> listTransactions(final boolean isFinished) {
        return interval.extract(it -> it.isFinished() == isFinished);
    }

    public final void deleteStoppedTransactions() {
        interval.forEachSecond(holder -> holder.removeIf((id, tx) -> tx.isFinished()));
    }
}
