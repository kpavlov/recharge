package com.example.recharge.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.OffsetDateTime;

@ResponseStatus(code = HttpStatus.NO_CONTENT, reason = "TransactionModel is too old")
public class TransactionTooOldException extends RuntimeException {
    public TransactionTooOldException(OffsetDateTime timestamp) {
        super("TransactionModel is too old: " + timestamp);
    }
}
