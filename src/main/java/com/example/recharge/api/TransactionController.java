package com.example.recharge.api;

import com.example.recharge.api.model.StopTransactionRequest;
import com.example.recharge.api.model.SubmitTransactionRequest;
import com.example.recharge.api.model.Totals;
import com.example.recharge.api.model.Transaction;
import com.example.recharge.api.model.TransactionListResponse;
import com.example.recharge.domain.TransactionModel;
import com.example.recharge.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.time.Clock;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class TransactionController implements TransactionsApi {

    private static final ResponseEntity<Void> NO_CONTENT_RESPONSE = ResponseEntity.noContent().build();
    private static final ResponseEntity<Void> OK_RESPONSE = ResponseEntity.ok().build();

    private final TransactionService service;
    private final Clock clock;

    public TransactionController(TransactionService service, Clock clock) {
        this.service = service;
        this.clock = clock;
    }

    @Override
    public ResponseEntity<Void> deleteStoppedTransactions() {
        service.deleteStoppedTransactions();
        return NO_CONTENT_RESPONSE;
    }

    @Override
    public ResponseEntity<TransactionListResponse> listStartedTransactions() {
        return createListResponse(service.listTransactions(false));
    }

    @Override
    public ResponseEntity<TransactionListResponse> listStoppedTransactions() {
        return createListResponse(service.listTransactions(true));
    }

    @Override
    public ResponseEntity<TransactionListResponse> listTransactions() {
        return createListResponse(service.listTransactions());
    }

    private ResponseEntity<TransactionListResponse> createListResponse(List<TransactionModel> transactions) {

        final List<Transaction> data = new ArrayList<>(transactions.size());
        int stopped = 0;
        for (TransactionModel transaction : transactions) {
            data.add(ApiConverter.convertTransaction(transaction));
            if (transaction.isFinished()) {
                stopped++;
            }
        }


        final Totals totals = new Totals()
            .totalCount(data.size())
            .totalStopped(stopped)
            .totalStarted(data.size() - stopped);


        final TransactionListResponse body = new TransactionListResponse()
            .data(data)
            .meta(totals);


        return ResponseEntity.ok(body);
    }

    @Override
    public ResponseEntity<Void> stopTransaction(@Valid @RequestBody StopTransactionRequest request) {
        service.stopTransaction(request.getId(), currentTime(), request.getConsumption());
        return OK_RESPONSE;
    }

    @Override
    public ResponseEntity<Void> submitTransaction(@Valid @RequestBody SubmitTransactionRequest request) {
        UUID createdTransactionId = service.createTransaction(currentTime(), request.getStationId());
        return ResponseEntity.created(URI.create("/transactions/" + createdTransactionId)).build();
    }

    private OffsetDateTime currentTime() {
        return clock.instant().atOffset(ZoneOffset.UTC);
    }
}
