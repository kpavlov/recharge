package com.example.recharge.api;

import com.example.recharge.api.model.Transaction;
import com.example.recharge.domain.TransactionModel;

class ApiConverter {

    public static Transaction convertTransaction(TransactionModel src) {
        Transaction result = new Transaction();
        result.setId(src.getId());
        result.setStationId(src.getStationId());
        result.setStartedAt(src.getStartedAt());
        if (src.isFinished()) {
            result.setStatus(Transaction.StatusEnum.FINISHED);
            result.setEndedAt(src.getEndedAt());
            result.setConsumption(src.getConsumption());
        } else {
            result.setStatus(Transaction.StatusEnum.IN_PROGRESS);
        }
        return result;
    }
}
