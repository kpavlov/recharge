package com.example.recharge.utils

import com.example.recharge.service.TransactionInTheFutureException
import com.example.recharge.service.TransactionTooOldException
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Clock
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit
import kotlin.random.Random.Default.nextInt
import kotlin.test.assertFailsWith

private const val TOTAL_SECONDS = 3

@ExtendWith(MockKExtension::class)
internal class TimeIntervalTest {

    @MockK
    private lateinit var clock: Clock
    private lateinit var currentTime: OffsetDateTime
    private var currentTimeMillis = 0L
    private var currentEpochSecond = 0L

    private lateinit var subject: TimeInterval<String, Int>

    @BeforeEach
    fun beforeEach() {
        currentTime = OffsetDateTime.now(Clock.systemUTC())
        currentTimeMillis = currentTime.toInstant().toEpochMilli()
        currentEpochSecond = currentTime.toEpochSecond()
        every { clock.millis() } returns currentTimeMillis

        subject = TimeInterval(
            TOTAL_SECONDS,
            clock)
    }

    @Test
    fun `Should be initially EMPTY`() {
        val result = extractAllValues()
        assertThat(result).isEmpty()
    }

    @Test
    fun `Should submit and get one value with current timestamp`() {
        // given
        val value = nextInt()
        //when
        subject.submit(currentTime, "$value", value)
        // then
        val result = extractAllValues()
        assertThat(result).containsExactly(value)
    }

    @Test
    fun `Should submit and get two values with current timestamp`() {
        // given
        val value1 = 10
        val value2 = 20
        //when
        subject.submit(currentTime, "$value1", value1)
        subject.submit(currentTime, "$value2", value2)

        // then
        val result = extractAllValues()

        assertThat(result).containsExactlyInAnyOrder(value1, value2)
    }


    @Test
    fun `Submit 3 values to different seconds`() {
        // given
        val amount1 = 10
        val amount2 = 20
        val amount3 = 30
        //when
        subject.submit(currentTime.minusSeconds(1), "$amount2", amount2)
        subject.submit(currentTime, "$amount1", amount1)
        subject.submit(currentTime.minusSeconds(2), "$amount3", amount3)


        // then
        val result = extractAllValues()
        assertThat(result).containsExactlyInAnyOrder(amount3, amount2, amount1)
    }

    @Test
    fun `Submit 1 value per second 3 times`() {
        // given
        val amount1 = 10
        val amount2 = 20
        val amount3 = 30
        //when
        waitNSecondsAndSubmit(0, amount1)
        waitNSecondsAndSubmit(1, amount2)
        waitNSecondsAndSubmit(2, amount3)

        // then
        val result = extractAllValues()
        assertThat(result).containsExactly(amount1, amount2, amount3)
    }

    @Test
    fun `Should discard old values (Submit values every 4 seconds - should have recent 3)`() {
        // given
        val amount0 = 100500
        val amount1 = 10
        val amount2 = 20
        val amount3 = 30

        //when
        waitNSecondsAndSubmit(0, amount0)

        waitNSecondsAndSubmit(1, amount1)
        waitNSecondsAndSubmit(2, amount2)
        waitNSecondsAndSubmit(3, amount3)

        // then
        val result = extractAllValues()

        assertThat(result).containsExactly(amount1, amount2, amount3)
    }

    @Test
    fun `Should reset values`() {
        // given
        subject.submit(currentTime, "10", 10)
        subject.submit(currentTime.minusSeconds(1), "20", 20)
        subject.submit(currentTime.minusSeconds(2), "30", 30)
        //when
        subject.reset()
        // then
        val result = extractAllValues()
        assertThat(result).isEmpty()
    }

    @Test
    fun `Should not accept values in the past`() {
        assertFailsWith(TransactionTooOldException::class) {
            //when-then
            subject.submit(currentTime.minusSeconds(TOTAL_SECONDS.toLong()), "10", 10)
        }
    }

    @Test
    fun `Should not accept value in the future`() {
        assertFailsWith(TransactionInTheFutureException::class) {
            //when-then
            subject.submit(currentTime.plus(1, ChronoUnit.MILLIS), "10", 10)
        }
    }

    @Test
    fun `Should perform operation for each second`() {
        // given
        subject.submit(currentTime, "10", 10)
        subject.submit(currentTime.minusSeconds(1), "20", 20)
        subject.submit(currentTime.minusSeconds(2), "30", 30)

        var sum = 0
        var count = 0
        // when
        subject.forEachSecond {
            sum += it.values().first()
            count++
        }
        // then
        assertThat(count).isEqualTo(TOTAL_SECONDS)
        assertThat(sum).isEqualTo(10 + 20 + 30)
    }

    private fun extractAllValues() = subject.extract { true }

    private fun waitNSecondsAndSubmit(seconds: Int, value: Int) {
        every { clock.millis() } returns currentTimeMillis + seconds * 1000
        subject.submit(currentTime.plusSeconds(seconds.toLong()), "$value", value)
    }

}
