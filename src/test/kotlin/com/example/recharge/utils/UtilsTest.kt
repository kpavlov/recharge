package com.example.recharge.utils

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Clock

@ExtendWith(MockKExtension::class)
internal class UtilsTest {

    @MockK
    private lateinit var clock: Clock

    @Test
    fun `Should get Epoch second`() {
        // given
        val currentTimeMillis = System.currentTimeMillis()
        every { clock.millis() } returns currentTimeMillis
        // when-then
        assertThat(Utils.getEpochSecond(clock)).isEqualTo(currentTimeMillis / 1000)
    }
}
