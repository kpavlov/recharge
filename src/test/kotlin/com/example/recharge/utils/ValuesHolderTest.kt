package com.example.recharge.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class ValuesHolderTest {

    private lateinit var subject: ValuesHolder<Int, String>

    @BeforeEach
    fun setUp() {
        subject = ValuesHolder(System.currentTimeMillis())
    }

    @Test
    fun `Should remove on condition`() {
        //given
        subject.put(1, "a")
        subject.put(2, "b")
        subject.put(3, "c")
        subject.put(4, "d")

        // when
        subject.removeIf { key, _ -> key % 2 == 0 }

        // then
        assertThat(subject.keys()).containsOnly(1, 3)
    }
}
