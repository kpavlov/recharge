package com.example.recharge.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.slf4j.LoggerFactory
import kotlin.random.Random.Default.nextInt
import kotlin.streams.toList

private val logger = LoggerFactory.getLogger(RingBufferTest::class.java)

private const val size: Int = 7

internal class RingBufferTest {

    private lateinit var subject: RingBuffer<Int>

    @BeforeEach
    fun setUp() {
        subject = RingBuffer(size) { it }
    }

    @Test
    fun `should initialize RingBuffer`() {
        assertThat(subject.size).isEqualTo(size)
        for (i in 0 until size) {
            assertThat(subject[i]).isEqualTo(i)
        }
        logger.debug(subject.toString())
    }

    @Test
    fun `should take elements matching predicate`() {
        // given
        val predicate: (Int) -> Boolean = { it % 2 == 1 }
        // when
        val result = subject.stream().filter(predicate).toList()
        // then
        assertThat(result).isNotEmpty
        assertThat(result).allMatch(predicate)
    }

    @Test
    fun `should advance RingBuffer by one step`() {
        logger.debug(subject.toString())
        val initialValue = -nextInt(10)
        for (i in 0 until size) {
            // first element is at `i`-th position
            assertThat(subject[i]).isEqualTo(0)
            // last element which has to be removed

            // when
            subject.advance(1) { initialValue }

            // then
            logger.debug("{}", subject)

            // elements should be shifted `i+1` times
            for (j in i + 1 until size) {
                assertThat(subject[j]).`as`("element[$j]").isEqualTo(j - i - 1)
            }
            // new elements should be `initialValue`
            for (j in 0 until i) {
                assertThat(subject[j]).`as`("element[$j").isEqualTo(initialValue)
            }
        }

        // all elements should have `initialValue`
        for (i in 0 until size) {
            assertThat(subject[i]).`as`("element at ${i + 1}").isEqualTo(initialValue)
        }
    }

    @ParameterizedTest(name = "advance by {0} steps")
    @ValueSource(ints = [1, 2, 3, 4, 5, 6, 7, 8, 9])
    fun `should advance RingBuffer by N steps`(steps: Int) {
        logger.debug("Before: {}", subject)

        // when
        subject.advance(steps) { -(it + 1) }

        // then
        logger.debug("After: {}", subject)
        // elements should be shifted `steps` times
        for (j in steps until size) {
            assertThat(subject[j]).`as`("element[$j]").isEqualTo(j - steps)
        }
        // new elements should have `initialValue`
        for (j in 0 until steps - 1) {
            assertThat(subject[j]).`as`("element[$j").isNegative()
        }
    }

    @Test
    fun `should reset RingBuffer`() {
        // given
        subject.advance(size - 1) { (it + 1) }
        logger.debug("Before: {}", subject)

        // when
        subject.reset { it }
        // then
        logger.debug("After: {}", subject)
        for (j in 0 until size) {
            assertThat(subject[j]).`as`("element[$j]").isEqualTo(j)
        }
    }

}
