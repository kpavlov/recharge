package com.example.recharge.api

import com.example.recharge.api.model.StopTransactionRequest
import com.example.recharge.api.model.SubmitTransactionRequest
import com.example.recharge.api.model.TransactionListResponse
import com.example.recharge.domain.TransactionModel
import com.example.recharge.service.TransactionService
import com.example.recharge.test.randomStartedTransactionModel
import com.example.recharge.test.randomStationId
import com.example.recharge.test.randomStoppedTransactionModel
import com.example.recharge.test.randomTransactionId
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.apache.http.HttpHeaders
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.time.Clock
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*
import kotlin.random.Random.Default.nextLong


@ExtendWith(MockKExtension::class)
internal class TransactionControllerTest {

    @MockK(relaxUnitFun = true)
    private lateinit var service: TransactionService

    @MockK
    private lateinit var clock: Clock

    @InjectMockKs
    private lateinit var subject: TransactionController

    private var currentTimeMillis = -1L

    private lateinit var currentTime: OffsetDateTime

    private var stationId: Int = -1

    @BeforeEach
    fun beforeEach() {
        stationId = randomStationId()
        currentTimeMillis = System.currentTimeMillis()
        currentTime = Date(currentTimeMillis).toInstant().atOffset(ZoneOffset.UTC)

        every { clock.millis() } returns currentTimeMillis
        every { clock.instant() } returns currentTime.toInstant()
    }

    @Test
    fun `Should create transaction`() {
        //given
        val newTransactionId = randomTransactionId()
        every { service.createTransaction(currentTime, stationId) } returns newTransactionId
        // when
        val result = subject.submitTransaction(
            SubmitTransactionRequest().stationId(stationId)
        )
        // then
        assertThat(result.headers[HttpHeaders.LOCATION]).containsExactly("/transactions/$newTransactionId")
        assertThat(result.statusCode).isEqualTo(HttpStatus.CREATED)
    }

    @Test
    fun `Should stop transaction`() {
        //given
        val transactionId = randomTransactionId()
        val consumption = nextLong(1, 100500)
        // when
        val result = subject.stopTransaction(
            StopTransactionRequest().id(transactionId).consumption(consumption)
        )
        // then
        verify { service.stopTransaction(transactionId, currentTime, consumption) }
        assertThat(result.statusCode).isEqualTo(HttpStatus.OK)
    }

    @Test
    fun `Should delete stopped transactions`() {
        // when
        val result = subject.deleteStoppedTransactions()
        // then
        verify { service.deleteStoppedTransactions() }
        assertThat(result.statusCode).isEqualTo(HttpStatus.NO_CONTENT)
    }

    @Test
    fun `Should get all transactions`() {
        // when
        val startedTx1 = randomStartedTransactionModel()
        val startedTx2 = randomStartedTransactionModel()
        val stoppedTx1 = randomStoppedTransactionModel()
        val stoppedTx2 = randomStoppedTransactionModel()
        val transactionList = listOf(
            startedTx1,
            stoppedTx2,
            stoppedTx1,
            startedTx2
        )
        every { service.listTransactions() } returns transactionList

        val result = subject.listTransactions()
        // then
        verifyTransactionListResponse(result, transactionList, 2, 2)
    }

    @Test
    fun `Should get started transactions`() {
        // when
        val startedTx1 = randomStartedTransactionModel()
        val startedTx2 = randomStartedTransactionModel()
        val transactionList = listOf(
            startedTx1,
            startedTx2
        )
        every { service.listTransactions(false) } returns transactionList

        val result = subject.listStartedTransactions()
        // then
        verifyTransactionListResponse(result, transactionList, 2, 0)
    }


    @Test
    fun `Should get stopped transactions`() {
        // when
        val stoppedTx1 = randomStoppedTransactionModel()
        val stoppedTx2 = randomStoppedTransactionModel()
        val transactionList = listOf(
            stoppedTx2,
            stoppedTx1
        )
        every { service.listTransactions(true) } returns transactionList

        val result = subject.listStoppedTransactions()
        // then
        verifyTransactionListResponse(result, transactionList, 0, 2)
    }

    private fun verifyTransactionListResponse(result: ResponseEntity<TransactionListResponse>,
                                              transactionList: List<TransactionModel>,
                                              expectedStartedCount: Int,
                                              expectedStoppedCount: Int) {
        assertThat(result.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(result.body).isNotNull
        with(result.body!!) {
            assertThat(this.meta.totalStarted).isEqualTo(expectedStartedCount)
            assertThat(this.meta.totalStopped).isEqualTo(expectedStoppedCount)
            assertThat(this.meta.totalCount).isEqualTo(transactionList.size)

            assertThat(this.data).hasSize(transactionList.size)
            transactionList.forEach { tx ->
                assertThat(this.data.filter { it.id == tx.id }).hasSize(1)
            }
        }
    }
}
