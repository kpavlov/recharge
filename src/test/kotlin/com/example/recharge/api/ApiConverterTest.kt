package com.example.recharge.api

import com.example.recharge.api.model.Transaction.StatusEnum.FINISHED
import com.example.recharge.api.model.Transaction.StatusEnum.IN_PROGRESS
import com.example.recharge.domain.TransactionModel
import com.example.recharge.test.randomStationId
import com.example.recharge.test.randomTransactionId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import kotlin.random.Random.Default.nextLong
import kotlin.test.assertNull

internal class ApiConverterTest {

    private val id = randomTransactionId()
    private val stationId = randomStationId()
    private val endedAt: OffsetDateTime = OffsetDateTime.now()
    private val startedAt = endedAt.minusMinutes(nextLong(1, 100500))
    private val consumption: Long = nextLong(1, 9999)

    @Test
    fun `Should convert started transaction`() {
        // given
        val src = TransactionModel(id, stationId, startedAt)

        // when
        val result = ApiConverter.convertTransaction(src)
        // then
        assertThat(result.id).isEqualTo(id)
        assertThat(result.stationId).isEqualTo(stationId)
        assertThat(result.startedAt).isEqualTo(startedAt)
        assertThat(result.status).isEqualTo(IN_PROGRESS)
        assertNull(result.endedAt)
        assertNull(result.consumption)
    }

    @Test
    fun `Should convert ended transaction`() {
        // given
        val src = TransactionModel(id, stationId, startedAt)
        src.finish(endedAt, consumption)

        // when
        val result = ApiConverter.convertTransaction(src)
        // then
        assertThat(result.id).isEqualTo(id)
        assertThat(result.stationId).isEqualTo(stationId)
        assertThat(result.startedAt).isEqualTo(startedAt)
        assertThat(result.status).isEqualTo(FINISHED)

        assertThat(result.endedAt).isEqualTo(endedAt)
        assertThat(result.consumption).isEqualTo(consumption)
    }
}
