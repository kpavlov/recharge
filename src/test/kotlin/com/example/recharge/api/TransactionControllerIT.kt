package com.example.recharge.api

import com.example.recharge.Application
import com.example.recharge.api.model.Transaction.StatusEnum.FINISHED
import com.example.recharge.api.model.Transaction.StatusEnum.IN_PROGRESS
import com.example.recharge.api.model.TransactionListResponse
import com.example.recharge.test.ApiTestClient
import com.example.recharge.utils.TimeInterval
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode.SAME_THREAD
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Clock
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*
import kotlin.test.assertNull
import kotlin.test.fail


@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [Application::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Execution(SAME_THREAD)
class TransactionControllerIT {

    @Autowired
    private lateinit var clock: Clock

    @Autowired
    private lateinit var timeInterval: TimeInterval<*, *>

    @LocalServerPort
    private var port: Int = -1

    private lateinit var client: ApiTestClient
    private lateinit var startTime: OffsetDateTime

    @BeforeAll
    fun beforeAll() {
        client = ApiTestClient(port)
    }

    private val inProgressTransactionIds = mutableListOf<UUID>()

    private val finishedTransactionIds = mutableListOf<UUID>()

    @BeforeEach
    fun beforeEach() {
        timeInterval.reset()
        startTime = clock.instant().atOffset(ZoneOffset.UTC)
        inProgressTransactionIds.clear()
        finishedTransactionIds.clear()
    }

    @Test
    fun `Should be initially empty`() {
        verifyEmpty(client.listTransactions().getOrThrow())
        verifyEmpty(client.listStartedTransactions().getOrThrow())
        verifyEmpty(client.listStoppedTransactions().getOrThrow())
    }

    private fun verifyEmpty(listResponse: TransactionListResponse) {
        assertThat(listResponse.meta.totalStopped).isEqualTo(0)
        assertThat(listResponse.meta.totalStarted).isEqualTo(0)
        assertThat(listResponse.meta.totalCount).isEqualTo(0)
    }

    @Test
    fun `Should submit and verify 10 Transaction`() {
        val transactionCount = 10
        createAndStopTransactions(transactionCount)

        val transactionListResponse = client.listTransactions().getOrThrow()

        assertThat(transactionListResponse).isNotNull
        assertThat(transactionListResponse.meta).isNotNull
        transactionListResponse.meta.let {
            assertThat(it.totalCount).isEqualTo(transactionCount)
            assertThat(it.totalStarted).isEqualTo(inProgressTransactionIds.size)
            assertThat(it.totalStopped).isEqualTo(finishedTransactionIds.size)
        }

        assertThat(transactionListResponse.data).isNotNull
        transactionListResponse.data.let {
            assertThat(it.size).isEqualTo(transactionCount)
            assertThat(it).allSatisfy { tx ->
                assertThat(tx.stationId).isGreaterThan(0)
                assertThat(tx.startedAt).isAfterOrEqualTo(startTime)
                when (tx.status) {
                    IN_PROGRESS -> {
                        assertThat(tx.id).isIn(inProgressTransactionIds)
                        assertNull(tx.endedAt)
                        assertNull(tx.consumption)
                    }
                    FINISHED -> {
                        assertThat(tx.id).isIn(finishedTransactionIds)
                        assertThat(tx.endedAt).isAfterOrEqualTo(tx.startedAt)
                        assertThat(tx.consumption).isGreaterThan(0)
                    }
                    else -> fail("Unexpected transaction status. Tx: $tx")
                }
            }
        }
    }

    private fun createAndStopTransactions(transactionCount: Int) {
        repeat(transactionCount) {
            val id = client.createTransaction(it + 1).getOrThrow()
            if (it % 2 == 0) {
                client.stopTransaction(id, (it + 1) * 100L)
                finishedTransactionIds.add(id)
            } else {
                inProgressTransactionIds.add(id)
            }
        }
    }

    @Test
    fun `Should delete stopped Transactions`() {
        // given
        val transactionCount = 10
        repeat(transactionCount) {
            val id = client.createTransaction(it + 1).getOrThrow()
            inProgressTransactionIds.add(id)
        }

        for (i in 0..inProgressTransactionIds.size step 3) {
            val id = inProgressTransactionIds[i]
            println(i)
            client.stopTransaction(id, 100500)
            finishedTransactionIds.add(id)
        }
        inProgressTransactionIds.removeAll(finishedTransactionIds)

        //when
        client.deleteStoppedTransactions()
        //then
        val transactionListResponse = client.listTransactions().getOrThrow()

        assertThat(transactionListResponse).isNotNull
        assertThat(transactionListResponse.meta).isNotNull
        transactionListResponse.meta.let {
            assertThat(it.totalCount).isEqualTo(inProgressTransactionIds.size)
            assertThat(it.totalStarted).isEqualTo(inProgressTransactionIds.size)
            assertThat(it.totalStopped).isEqualTo(0)
        }

        assertThat(transactionListResponse.data).isNotNull
        transactionListResponse.data.let {
            assertThat(it.size).isEqualTo(inProgressTransactionIds.size)
            assertThat(it).allSatisfy { tx ->
                assertThat(tx.stationId).isGreaterThan(0)
                assertThat(tx.status).isEqualTo(IN_PROGRESS)
            }
        }
    }

    @Test
    fun `Should NOT stop unknown Transaction`() {
        client.stopTransaction(UUID.randomUUID(), 1, expectedStatus = HttpStatus.NOT_FOUND)
    }

}
