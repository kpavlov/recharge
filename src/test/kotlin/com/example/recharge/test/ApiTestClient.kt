package com.example.recharge.test

import com.example.recharge.api.model.StopTransactionRequest
import com.example.recharge.api.model.SubmitTransactionRequest
import com.example.recharge.api.model.TransactionListResponse
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.util.*

class ApiTestClient(port: Int) {

    init {
        RestAssured.port = port
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }

    /**
     *
     */
    fun createTransaction(stationId: Int?): Result<UUID> {
        return Result.runCatching {
            val requestBody = SubmitTransactionRequest().stationId(stationId)
            val location = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post("/transactions")
                .then()
                .statusCode(201)
                .assertThat()
                .header(HttpHeaders.LOCATION, CoreMatchers.startsWith("/transactions/"))
                .extract().header(HttpHeaders.LOCATION)
            val idString = location.substringAfterLast("/")
            UUID.fromString(idString)
        }
    }

    fun listTransactions(): Result<TransactionListResponse> {
        return Result.runCatching {
            RestAssured
                .given()
                .get("/transactions")
                .then()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .extract().body().`as`(TransactionListResponse::class.java)
        }
    }

    fun listStartedTransactions(): Result<TransactionListResponse> {
        return Result.runCatching {
            RestAssured
                .given()
                .get("/transactions/started")
                .then()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .extract().body().`as`(TransactionListResponse::class.java)
        }
    }

    fun listStoppedTransactions(): Result<TransactionListResponse> {
        return Result.runCatching {
            RestAssured
                .given()
                .get("/transactions/stopped")
                .then()
                .statusCode(HttpStatus.OK.value())
                .assertThat()
                .extract().body().`as`(TransactionListResponse::class.java)
        }
    }

    fun deleteStoppedTransactions() {
        RestAssured
            .given()
            .delete("/transactions")
            .then()
            .statusCode(HttpStatus.NO_CONTENT.value())
    }

    fun stopTransaction(id: UUID, consumption: Long, expectedStatus: HttpStatus = HttpStatus.OK) {
        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .body(StopTransactionRequest().apply {
                this.id = id
                this.consumption = consumption
            })
            .put("/transaction")
            .then()
            .statusCode(expectedStatus.value())
    }
}
