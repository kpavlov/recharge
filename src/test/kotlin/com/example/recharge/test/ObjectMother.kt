package com.example.recharge.test

/**
 * Read more about <a href="https://martinfowler.com/bliki/ObjectMother.html">Object Mother</a>
 */
import com.example.recharge.domain.TransactionModel
import java.time.OffsetDateTime
import java.util.*
import kotlin.random.Random.Default.nextInt
import kotlin.random.Random.Default.nextLong

fun randomTransactionId() = UUID.randomUUID()!!
fun randomStationId() = nextInt(1, 100500)

fun randomStartedTransactionModel() = TransactionModel(
    randomTransactionId(),
    randomStationId(),
    OffsetDateTime.now().minusSeconds(nextLong(1, 100))
)

fun randomStoppedTransactionModel() = TransactionModel(
    randomTransactionId(),
    randomStationId(),
    OffsetDateTime.now().minusMinutes(nextLong(10, 100))
).apply { finish(OffsetDateTime.now().minusSeconds(nextLong(1, 100)), nextLong(1, 100500)) }
