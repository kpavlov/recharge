package com.example.recharge.domain

import com.example.recharge.test.randomStationId
import com.example.recharge.test.randomTransactionId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.OffsetDateTime
import kotlin.random.Random.Default.nextLong
import kotlin.test.assertNull

internal class TransactionModelTest {

    private val transactionId = randomTransactionId()
    private val stationId = randomStationId()
    private val startTime = OffsetDateTime.now().minusMinutes(nextLong(1, 100))
    private val endTime = OffsetDateTime.now()
    private val consumption = nextLong(1, 100500)

    @Test
    fun `Should create in-progress transaction`() {
        val subject = TransactionModel(transactionId, stationId, startTime)
        assertThat(subject.isFinished).isFalse()
        assertThat(subject.id).isEqualTo(transactionId)
        assertThat(subject.stationId).isEqualTo(stationId)
        assertThat(subject.startedAt).isEqualTo(startTime)
        assertNull(subject.endedAt)
        assertNull(subject.consumption)
    }

    @Test
    fun `Should finish transaction`() {
        val subject = TransactionModel(transactionId, stationId, startTime)
        // when
        subject.finish(endTime, consumption)
        //then
        assertThat(subject.isFinished).isTrue()
        assertThat(subject.endedAt).isEqualTo(endTime)
        assertThat(subject.consumption).isEqualTo(consumption)
    }

    @Test
    fun `Should not finish transaction twice`() {
        //given
        val subject = TransactionModel(transactionId, stationId, startTime)
        subject.finish(endTime, consumption)

        //then
        assertThrows<IllegalStateException> {
            // when
            subject.finish(endTime, consumption)
        }
    }

    @Test
    fun `Should not finish transaction in the past`() {
        //given
        val subject = TransactionModel(transactionId, stationId, startTime)

        //then
        assertThrows<IllegalArgumentException> {
            // when
            subject.finish(startTime.minusSeconds(1), consumption)
        }
    }
}
